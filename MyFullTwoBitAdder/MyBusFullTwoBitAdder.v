`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date:    15:33:56 10/29/2013 
// Design Name: 
// Module Name:    MyBusFullTwoBitAdder 
// Project Name: 
// Target Devices: 
// Tool versions: 
// Description: 
//
// Dependencies: 
//
// Revision: 
// Revision 0.01 - File Created
// Additional Comments: 
//
//////////////////////////////////////////////////////////////////////////////////
module MyBusFullTwoBitAdder(
	a, b, q
	);

	input [1:0] a, b; // Input 2-bit numbers
	output [2:0] q;   // Output 3-bit number
	
	MyFullOneBitAdder FullOneBitAdder0(a[0], b[0], 0, q[0], c_out0);
	MyFullOneBitAdder FullOneBitAdder1(a[1], b[1], c_out0, q[1], q[2]);

endmodule
