`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date:    15:35:45 10/29/2013 
// Design Name: 
// Module Name:    MyBusFullFourBitAdderV1 
// Project Name: 
// Target Devices: 
// Tool versions: 
// Description: 
//
// Dependencies: 
//
// Revision: 
// Revision 0.01 - File Created
// Additional Comments: 
//
//////////////////////////////////////////////////////////////////////////////////
module MyBusFullFourBitAdderV1(
	Switches, LEDs
	);

	(* LOC = "P6 P10 P12 P18 P24 P29 P36 P38" *) input [7:0] Switches;
	(* LOC = "P2 P3 P4 P5 P7 P8 P14 P15" *) output [7:0] LEDs;

	// Split the input into the two numbers
	wire [3:0] a = Switches[3:0];
	wire [3:0] b = Switches[7:4];
	
	wire [4:0] q;
	
	MyFullOneBitAdder FullOneBitAdder0(a[0], b[0], 0,      q[0], c_out0);
	MyFullOneBitAdder FullOneBitAdder1(a[1], b[1], c_out0, q[1], c_out1);
	MyFullOneBitAdder FullOneBitAdder2(a[2], b[2], c_out1, q[2], c_out2);
	MyFullOneBitAdder FullOneBitAdder3(a[3], b[3], c_out2, q[3], q[4]);
	
	assign LEDs = {3'b000, q};
	
endmodule
