`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date:    15:11:41 10/29/2013 
// Design Name: 
// Module Name:    MyFullFourBitAdder 
// Project Name: 
// Target Devices: 
// Tool versions: 
// Description: 
//
// Dependencies: 
//
// Revision: 
// Revision 0.01 - File Created
// Additional Comments: 
//
//////////////////////////////////////////////////////////////////////////////////
module MyFullFourBitAdder(
	a0, a1, a2, a3, // First four-bit number
	b0, b1, b2, b3, // Second four-bit number
	q0, q1, q2, q3, c_out // Output four-bit number and carry flag
	);

	(* LOC = "P38" *) input a0;
	(* LOC = "P36" *) input a1;
	(* LOC = "P29" *) input a2;
	(* LOC = "P24" *) input a3;
	(* LOC = "P18" *) input b0;
	(* LOC = "P12" *) input b1;
	(* LOC = "P10" *) input b2;
	(* LOC = "P6" *)  input b3;
	(* LOC = "P15" *) output q0;
	(* LOC = "P14" *) output q1;
	(* LOC = "P8" *)  output q2;
	(* LOC = "P7" *)  output q3;
	(* LOC = "P5" *)  output c_out;
	
	MyFullOneBitAdder FullOneBitAdder0(a0, b0, 0,      q0, c_out0);
	MyFullOneBitAdder FullOneBitAdder1(a1, b1, c_out0, q1, c_out1);
	MyFullOneBitAdder FullOneBitAdder2(a2, b2, c_out1, q2, c_out2);
	MyFullOneBitAdder FullOneBitAdder3(a3, b3, c_out2, q3, c_out);


endmodule
