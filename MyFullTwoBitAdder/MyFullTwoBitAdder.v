`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date:    15:04:57 10/29/2013 
// Design Name: 
// Module Name:    MyFullTwoBitAdder 
// Project Name: 
// Target Devices: 
// Tool versions: 
// Description: 
//
// Dependencies: 
//
// Revision: 
// Revision 0.01 - File Created
// Additional Comments: 
//
//////////////////////////////////////////////////////////////////////////////////
module MyFullTwoBitAdder(a0, a1, b0, b1, q0, q1, c_out);
	input a0, a1; // First 2-bit number
	input b0, b1; // Second 2-bit number
	output q0, q1, c_out; // Output 2-bit number and carry flag
	
	MyFullOneBitAdder FullOneBitAdder0(a0, b0, 0, q0, c_out0);
	MyFullOneBitAdder FullOneBitAdder1(a1, b1, c_out0, q1, c_out);

endmodule
