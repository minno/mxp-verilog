`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date:    14:46:07 10/31/2013 
// Design Name: 
// Module Name:    DecadeCounter 
// Project Name: 
// Target Devices: 
// Tool versions: 
// Description: 
//
// Dependencies: 
//
// Revision: 
// Revision 0.01 - File Created
// Additional Comments: 
//
//////////////////////////////////////////////////////////////////////////////////
module DecadeCounter(clk, LEDs, c_out);
	(* CLOCK_DEDICATED_ROUTE = "FALSE", LOC = "P38" *) input clk;
	(* LOC = "P7 P8 P14 P15" *) output [3:0] LEDs;
	(* LOC = "P5" *) output c_out;
	
	reg [3:0] count;
	reg c_out_reg;
	
	always @(posedge clk) begin
		if (count == 9) begin
			count <= 0;
			c_out_reg <= 1;
		end
		else begin
			count <= count + 1;
			c_out_reg <= 0;
		end
	end
	
	assign LEDs = count;
	assign c_out = c_out_reg;

endmodule
