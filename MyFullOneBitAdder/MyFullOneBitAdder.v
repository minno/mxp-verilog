`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date:    14:56:30 10/29/2013 
// Design Name: 
// Module Name:    MyFullOneBitAdder 
// Project Name: 
// Target Devices: 
// Tool versions: 
// Description: 
//
// Dependencies: 
//
// Revision: 
// Revision 0.01 - File Created
// Additional Comments: 
//
//////////////////////////////////////////////////////////////////////////////////
module MyFullOneBitAdder(
	(* LOC = "P38" *) input a,
	(* LOC = "P18" *) input b,
	(* LOC = "P6" *) input c_in,
	(* LOC = "P15" *) output q,
	(* LOC = "P14" *) output c_out
	);
	
	assign q = a ^ b ^ c_in;
	assign c_out = (a & b) | (a & c_in) | (b & c_in);

endmodule
