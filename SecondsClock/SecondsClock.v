`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date:    14:57:10 10/31/2013 
// Design Name: 
// Module Name:    SecondsClock 
// Project Name: 
// Target Devices: 
// Tool versions: 
// Description: 
//
// Dependencies: 
//
// Revision: 
// Revision 0.01 - File Created
// Additional Comments: 
//
//////////////////////////////////////////////////////////////////////////////////
module SecondsClock(clk, out);
	(* LOC = "P54" *) input clk;
	(* LOC = "P15" *) output out;
	
	reg [31:0] count;
	reg out_reg;
	
	always @(posedge clk) begin
		if (count < 24_999_999) begin
			count <= count + 1;
		end else begin
			count <= 0;
			out_reg <= !out_reg;
		end
	end
	
	assign out = out_reg;

endmodule
