`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date:    15:01:43 11/05/2013 
// Design Name: 
// Module Name:    Main 
// Project Name: 
// Target Devices: 
// Tool versions: 
// Description: 
//
// Dependencies: 
//
// Revision: 
// Revision 0.01 - File Created
// Additional Comments: 
//
//////////////////////////////////////////////////////////////////////////////////
module Main(sys_clk, pin_out);
	(* LOC = "P54" *) input sys_clk;
	(* LOC = "P81" *) output pin_out;
	
	AudioClock clk (sys_clk, pin_out);

endmodule
