`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date:    15:01:31 11/05/2013 
// Design Name: 
// Module Name:    AudioClock 
// Project Name: 
// Target Devices: 
// Tool versions: 
// Description: 
//
// Dependencies: 
//
// Revision: 
// Revision 0.01 - File Created
// Additional Comments: 
//
//////////////////////////////////////////////////////////////////////////////////
module AudioClock(sys_clk, out);
	input sys_clk;
	output reg out;
	
	parameter z = 25_000_000;
	parameter y = 44_100;
	parameter SIGMA_BITS = 25;
	
	reg [SIGMA_BITS-1:0] sigma;

	always @(posedge sys_clk) begin
		if (sigma >= z) begin
			sigma <= sigma + y - z;
			out <= 1;
		end else begin
			sigma <= sigma + y;
			out <= 0;
		end
	end

endmodule
