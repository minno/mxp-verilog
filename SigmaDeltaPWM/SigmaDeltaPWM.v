`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date:    15:19:13 11/05/2013 
// Design Name: 
// Module Name:    SigmaDeltaPWM 
// Project Name: 
// Target Devices: 
// Tool versions: 
// Description: 
//
// Dependencies: 
//
// Revision: 
// Revision 0.01 - File Created
// Additional Comments: 
//
//////////////////////////////////////////////////////////////////////////////////
module SigmaDeltaPWM(clk, control, out);
	parameter MAXBITS = 8;
	input clk;
	input [MAXBITS-1:0] control;
	output out;
	
	reg [MAXBITS:0] sigma = 0;
	
	always @(posedge clk) begin
		sigma <= sigma[MAXBITS-1:0] + control;
	end
	
	assign out = sigma[MAXBITS];

endmodule
