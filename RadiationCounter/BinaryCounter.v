`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date:    15:07:11 10/31/2013 
// Design Name: 
// Module Name:    BinaryCounter 
// Project Name: 
// Target Devices: 
// Tool versions: 
// Description: 
//
// Dependencies: 
//
// Revision: 
// Revision 0.01 - File Created
// Additional Comments: 
//
//////////////////////////////////////////////////////////////////////////////////

// Counts the number positive edges in "signal" up to 16 bits
// resets to zero on overflow or while "reset" is HI
module BinaryCounter(signal, out, reset);
	input signal;
	input reset;
	output [15:0] out;
	
	reg [15:0] count;

	always @(posedge signal or posedge reset) begin
		if (reset) begin
			count <= 0;
		end else begin
			count <= count+1;
		end
	end
	assign out = count;

endmodule
