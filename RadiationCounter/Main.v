`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date:    15:10:02 10/31/2013 
// Design Name: 
// Module Name:    Main 
// Project Name: 
// Target Devices: 
// Tool versions: 
// Description: 
//
// Dependencies: 
//
// Revision: 
// Revision 0.01 - File Created
// Additional Comments: 
//
//////////////////////////////////////////////////////////////////////////////////

// This module handles the connections between different modules and between modules and the external pins.
module Main(clk, rad_ctr, sevenSegLED_out, sevenSegPos_out);
	// Internal device clock, 25 MHz.
	(* LOC = "P54" *) input clk;
	
	// Measurement device input, leftmost pin on top row
	(* CLOCK_DEDICATED_ROUTE = "FALSE", LOC = "P81" *) input rad_ctr;
	
	// 7-segment display outputs
	(* LOC = "P83 P17 P20 P21 P23 P16 P25" *) output [6:0] sevenSegLED_out;
	(* LOC = "P26 P32 P33 P34" *) output [3:0] sevenSegPos_out;

	wire [15:0] count;
	wire [15:0] latch_out;

	SecondsClock Seconds(clk, seconds_out);
	BinaryCounter Counter(rad_ctr, count, seconds_out);
	
	StorageLatch Latch(seconds_out, count, latch_out);
	
	HexDisplayV1 Display(clk, latch_out, 1, 1, sevenSegLED_out, sevenSegPos_out);

endmodule
