`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date:    14:57:10 10/31/2013 
// Design Name: 
// Module Name:    SecondsClock 
// Project Name: 
// Target Devices: 
// Tool versions: 
// Description: 
//
// Dependencies: 
//
// Revision: 
// Revision 0.01 - File Created
// Additional Comments: 
//
//////////////////////////////////////////////////////////////////////////////////

// outputs LO for 25,000,000 cycles of clk (1 sec at 25 MHz) followed by HI for one cycle
module SecondsClock(clk, out);
	input clk;
	output out;
	
	reg [31:0] count;
	reg out_reg;
	
	always @(posedge clk) begin
		if (count < 25_000_000) begin
			count <= count + 1;
			out_reg <= 0;
		end else begin
			count <= 0;
			out_reg <= 1;
		end
	end
	
	assign out = out_reg;

endmodule
