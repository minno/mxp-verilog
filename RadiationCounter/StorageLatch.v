`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date:    15:44:12 10/31/2013 
// Design Name: 
// Module Name:    StorageLatch 
// Project Name: 
// Target Devices: 
// Tool versions: 
// Description: 
//
// Dependencies: 
//
// Revision: 
// Revision 0.01 - File Created
// Additional Comments: 
//
//////////////////////////////////////////////////////////////////////////////////

// outputs whatever "in" was during the last positive edge of "store".
module StorageLatch(store, in, out);
	input store;
	input [15:0] in;
	output [15:0] out;
	
	reg [15:0] storage;

	always @(posedge store) begin
		storage <= in;
	end
	
	assign out = storage;

endmodule
