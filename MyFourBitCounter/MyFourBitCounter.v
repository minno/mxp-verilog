`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date:    14:22:42 10/31/2013 
// Design Name: 
// Module Name:    MyFourBitCounter 
// Project Name: 
// Target Devices: 
// Tool versions: 
// Description: 
//
// Dependencies: 
//
// Revision: 
// Revision 0.01 - File Created
// Additional Comments: 
//
//////////////////////////////////////////////////////////////////////////////////
module MyFourBitCounter(
	clk, LEDs);
	(* CLOCK_DEDICATED_ROUTE = "FALSE", LOC = "P69" *) input clk;
	(* LOC = "P7 P8 P14 P15" *) output [3:0] LEDs;
	reg [3:0] count;

	always @(posedge clk) begin
		count <=count+1;
	end
	assign LEDs = count;

endmodule
