`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date:    14:41:15 11/07/2013 
// Design Name: 
// Module Name:    Main 
// Project Name: 
// Target Devices: 
// Tool versions: 
// Description: 
//
// Dependencies: 
//
// Revision: 
// Revision 0.01 - File Created
// Additional Comments: 
//
//////////////////////////////////////////////////////////////////////////////////

// Reads a 16-bit .wav file from a memory chip on the 2nd top bank of pins and outputs
// an audio signal to an audio amplifier connected to the 1st top bank of pins.
// Assumes the system clock is running at 25 MHz.
module Main(sys_clk, mem_Q, mem_D, NCS, mem_clk_out, audio_out);
	(* LOC = "P54" *) input sys_clk;
	(* LOC = "P88" *) input mem_Q;
	(* LOC = "P93" *) output mem_D;
	(* LOC = "P87" *) output NCS;
	(* LOC = "P94" *) output mem_clk_out;
	(* LOC = "P81" *) output audio_out;

	AudioClock aclk (sys_clk, audio_clock);
	
	wire [23:0] address;
	AddressCounter acount (audio_clock, address);
	wire [15:0] data;
	ReadTwoBytes mem_reader (sys_clk, audio_clock, address, mem_Q, NCS, mem_D, mem_clk_out, busy, data);
	
	SigmaDeltaPWM pwm (sys_clk, data, audio_out);

endmodule
