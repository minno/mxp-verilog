`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date:    14:27:32 11/05/2013 
// Design Name: 
// Module Name:    SimplePWM 
// Project Name: 
// Target Devices: 
// Tool versions: 
// Description: 
//
// Dependencies: 
//
// Revision: 
// Revision 0.01 - File Created
// Additional Comments: 
//
//////////////////////////////////////////////////////////////////////////////////
module SimplePWM(clk, control, out);
	parameter MAXBITS = 16;
	
	input clk;
	input [MAXBITS-1:0] control; // Fraction of time that pulse is on
	output reg out = 1;
	
	reg [MAXBITS-1:0] counter = 0;
	
	always @(posedge clk) begin
		if (counter < control) begin
			out <= 1;
		end else begin
			out <= 0;
		end
		counter <= counter + 1;
	end

endmodule
