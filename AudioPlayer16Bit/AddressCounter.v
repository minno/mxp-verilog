`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date:    14:36:00 11/07/2013 
// Design Name: 
// Module Name:    AddressCounter 
// Project Name: 
// Target Devices: 
// Tool versions: 
// Description: 
//
// Dependencies: 
//
// Revision: 
// Revision 0.01 - File Created
// Additional Comments: 
//
//////////////////////////////////////////////////////////////////////////////////

// Counts between 0 and 0x1FFFFF by 2 with every positive edge of sys_clk
module AddressCounter(sys_clk, address);
	input sys_clk;
	output reg [23:0] address;
	
	always @(posedge sys_clk) begin
		if (address == 24'h1f_ffff) begin
			address <= 0;
		end else begin
			address <= address + 2;
		end
	end

endmodule
