`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date:    14:30:23 10/29/2013 
// Design Name: 
// Module Name:    SimpleIO 
// Project Name: 
// Target Devices: 
// Tool versions: 
// Description: 
//
// Dependencies: 
//
// Revision: 
// Revision 0.01 - File Created
// Additional Comments: 
//
//////////////////////////////////////////////////////////////////////////////////
module SimpleIO(
	(* LOC = "P69" *) input a, // Rightmost button in lower-right corner
	(* LOC = "P15" *) output q // Rightmost LED next to 7-segment display
	);
	
	assign q = a;

endmodule
