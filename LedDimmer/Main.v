`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date:    14:34:35 11/05/2013 
// Design Name: 
// Module Name:    Main 
// Project Name: 
// Target Devices: 
// Tool versions: 
// Description: 
//
// Dependencies: 
//
// Revision: 
// Revision 0.01 - File Created
// Additional Comments: 
//
//////////////////////////////////////////////////////////////////////////////////
module Main(sys_clk, switches, LED, pin_out, ground);
	(* LOC = "P54" *) input sys_clk;
	(* LOC = "P6 P10 P12 P18 P24 P29 P36 P38" *) input [7:0] switches;
	(* LOC = "P15" *) output LED;
	(* LOC = "P81" *) output pin_out;
	(* LOC = "P91" *) output ground;
	
	Counter ctr (sys_clk, count_out);
	SimplePWM pwm (count_out, switches, LED);
	
	assign pin_out = LED;
	assign ground = 0;

endmodule
