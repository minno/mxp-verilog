`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date:    14:31:03 11/05/2013 
// Design Name: 
// Module Name:    Counter 
// Project Name: 
// Target Devices: 
// Tool versions: 
// Description: 
//
// Dependencies: 
//
// Revision: 
// Revision 0.01 - File Created
// Additional Comments: 
//
//////////////////////////////////////////////////////////////////////////////////
module Counter(sys_clk, out);
	input sys_clk;
	output out;
	
	reg [19:0] counter = 0;
	
	parameter n = 11;
	
	always @(posedge sys_clk) begin
		counter <= counter + 1;
	end
	
	assign out = counter[n];

endmodule
